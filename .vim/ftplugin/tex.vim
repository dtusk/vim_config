" compile to pdf

unmap <F9>

nmap <F9> :!clear<CR>:!pdflatex %<CR>
vmap <F9> <esc>:!clear<CR>:!pdflatex %<CR>
imap <F9> <esc>:!clear<CR>:!pdflatex %<CR>

" latexml-latexmlpost compile

unmap <F8>

let s:filename = expand('%:t:r')
let tex_xmlfile = s:filename . ".xml"
let tex_xhtmlfile = s:filename . ".xhtml"

nmap <F8> :!clear<CR>:execute '!latexml --dest /tmp/' . tex_xmlfile . ' %'<CR>:execute '!latexmlpost --dest ' . tex_xhtmlfile . ' /tmp/' . tex_xmlfile<CR>
vmap <F8> <esc>:!clear<CR>:execute '!latexml --dest /tmp/' . tex_xmlfile . ' %'<CR>:execute '!latexmlpost --dest ' . tex_xhtmlfile . ' /tmp/' . tex_xmlfile<CR>
imap <F8> <esc>:!clear<CR>:execute '!latexml --dest /tmp/' . tex_xmlfile . ' %'<CR>:execute '!latexmlpost --dest ' . tex_xhtmlfile . ' /tmp/' . tex_xmlfile<CR>

