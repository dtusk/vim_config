scriptencoding UTF-8

if has('win32') || has('win64')
    set runtimepath^=~/.vim
    set runtimepath+=~/.vim/after
endif

autocmd! bufwritepost .vimrc source %

filetype off
filetype plugin indent on
syntax on


set foldmethod=manual
set nofoldenable
" taby

set expandtab
set shiftround
set shiftwidth=2
set softtabstop=2
set tabstop=2

" szukanie

set hlsearch
set ignorecase
set incsearch
set smartcase

set nobackup
set nowritebackup
set noswapfile

set smartindent


set mouse=a
set bs=2

let mapleader = ","

" Wyłącza highlighting

"noremap <C-n> :nohl<CR>
"vnoremap <C-n> :nohl<CR>
"inoremap <C-n> <esc>:nohl<CR>

" Quicksave

noremap <C-Z> :update<CR>
vnoremap <C-Z> <esc>:update<CR>
inoremap <C-Z> <esc>:update<CR>

" Szybkie wylaczanie
noremap <Leader>e :quit<CR>
noremap <Leader>E :qa!<CR>

" taby
map <Leader>n <esc>:tabp<CR>
map <Leader>m <esc>:tabn<CR>

" make

nmap <F5> :!clear<CR>:make<CR>
vmap <F5> <esc>:!clear<CR>:make<CR>
imap <F5> <esc>:!clear<CR>:make<CR>
    
" make z run
nmap <F6> :!clear<CR>:make<CR>:make run<CR>
vmap <F6> <esc>:!clear<CR>:make<CR>:make run<CR>
imap <F6> <esc>:!clear<CR>:make<CR>:make run<CR>"


" sortowanie :)

vnoremap <Leader>s :sort<CR>

" przesuniecie blokowe

vnoremap < <gv
vnoremap > >gv

autocmd ColorScheme * hi ExtraWhitespace ctermbg=red guibg=red

" kolory
set t_Co=256
colo wombat256mod


set tw=79
set number
map <F2> :set number!<CR>
vmap <F2> <esc>:set number!<CR>
imap <F2> <esc>:set number!<CR>
set nowrap
set fo-=t
set colorcolumn=80
hi ColorColumn ctermbg=233

" formatowanie

vmap Q gq
nmap Q gqap


" patola

call pathogen#infect()

" powerline

set laststatus=2

" ctrlp

let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*.toc
set wildignore+=*.aux
set wildignore+=*_build/*
set wildignore+=*.lg

if has("gui_running")
    set guifont=Consolas:h11
endif


